# mobile_robot_kinematics

`.ride.yml` dosyasi 

```
advanced:
  custom_build: true
requirements:
  packages:
    - 'https://gitlab.com/talhaaliarslan/mobile_robot_kinematics'
runtime:
  launch:
    args:
      model_name: 'robot'
      x_pos: 1.0
      y_pos: 0.0
      wheel_spacing: 0.5
      wheel_diameter: 0.2
      wheel_shaft_offset: 0.0
      rear_caster_offset_input: -0.5
      start_gazebo: 'true'
    on_reset: rosservice call /gazebo/reset_world
    package: mobile_robot_kinematics
    path: spawn_robot.launch
```

```
sudo apt update

sudo apt install -y ros-melodic-teleop-twist-keyboard \ 
                    ros-melodic-diff-drive-controller
```



```
rosservice call /gazebo/delete_model robot
```

```
roslaunch mobile_robot_kinematics spawn_robot.launch x_pos:=0 y_pos:=0 rear_caster_offset_input:='-0.25' wheel_diameter:=0.15 wheel_shaft_offset:=0.1 wheel_spacing:=0.5 
```

```
rosrun teleop_twist_keyboard teleop_twist_keyboard.py cmd_vel:=/mobile_base_controller/cmd_vel
```